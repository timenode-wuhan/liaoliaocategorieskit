//
//  CategoriesKit
//  UIColor+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Category)

/// 窗口颜色
+ (UIColor *)mainWindowBackColor
NS_SWIFT_NAME(windowBack());

/// 333 深黑色
+ (UIColor *)mainBlackColor333
NS_SWIFT_NAME(black333());

/// 666 浅黑色
+ (UIColor *)mainBlackColor666
NS_SWIFT_NAME(black666());

/// 999 浅灰色
+ (UIColor *)mainBlackColor999
NS_SWIFT_NAME(black999());

/// 红色
+ (UIColor *)mainRedColor
NS_SWIFT_NAME(mainRed());

/// 主题紫
+ (UIColor *)mainPurpleColor
NS_SWIFT_NAME(mainPurple());

/// 亮灰色
+ (UIColor *)mainLightDarkColor
NS_SWIFT_NAME(mainLightDark());

/// 按钮未选中状态背景颜色
+ (UIColor *)mainUnableColor
NS_SWIFT_NAME(mainUnable());

/// 分割线颜色
+ (UIColor *)mainSeparatorColor
NS_SWIFT_NAME(mainSeparator());

/// 文字颜色（副标题）
+ (UIColor *)grayTextColor
NS_SWIFT_NAME(grayText());

/// 男性标签背景颜色
+ (UIColor *)maleBagdeColor
NS_SWIFT_NAME(maleBagde());

/// 女性标签背景颜色
+ (UIColor *)femaleBagdeColor
NS_SWIFT_NAME(femaleBagde());

/// 生成颜色
/// - Parameter color: 16进制颜色字符
+ (UIColor *)func_colorWithHexString:(NSString *)color
NS_SWIFT_NAME(func_colorWithHexString(color:));

/// 生成颜色
/// - Parameters:
///   - light: 亮色模式
///   - dark: 暗黑模式
+ (UIColor *)func_colorWithColorLight:(UIColor *)light dark:(UIColor *)dark
NS_SWIFT_NAME(func_colorWithColorLight(light:dark:));

/// 生成颜色
/// - Parameter value: rgba
+ (UIColor *)func_rgbColorWithValue:(int)value
NS_SWIFT_NAME(func_rgbColorWithValue(value:));


@end

NS_ASSUME_NONNULL_END
