//
//  CategoriesKit
//  UIColor+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIColor+Category.h"

@implementation UIColor (Category)

+ (UIColor *)mainWindowBackColor {
    return [self func_rgbColorWithValue:0xF9F9F9];
}

+ (UIColor *)mainBlackColor333 {
    return [self func_rgbColorWithValue:0x333333];
}

+ (UIColor *)mainBlackColor666 {
    return [self func_rgbColorWithValue:0x666666];
}

+ (UIColor *)mainBlackColor999 {
    return [self func_rgbColorWithValue:0x999999];
}

+ (UIColor *)mainPurpleColor {
    return [self func_rgbColorWithValue:0x7244DD];
}

+ (UIColor *)grayTextColor {
    return [self func_rgbColorWithValue:0xAAAAAA];
}

+ (UIColor *)mainSeparatorColor {
    return [self func_rgbColorWithValue:0xDADADA];
}

+ (UIColor *)mainRedColor {
    return [self func_rgbColorWithValue:0xFF2D2D];
}

+ (UIColor *)mainLightDarkColor {
    return [self func_rgbColorWithValue:0xCCCCCC];
}

+ (UIColor *)mainUnableColor {
    return [self func_rgbColorWithValue:0xD9DEE6];
}

/// 男性标签背景颜色
+ (UIColor *)maleBagdeColor {
    return [self func_rgbColorWithValue:0x597EF7];
}

/// 女性标签背景颜色
+ (UIColor *)femaleBagdeColor {
    return [self func_rgbColorWithValue:0xD81159];
}

/// 生成颜色
/// - Parameters:
///   - light: 亮色模式
///   - dark: 暗黑模式
+ (UIColor *)func_colorWithColorLight:(UIColor *)light dark:(UIColor *)dark {
    if (@available(iOS 13.0, *)) {
        return [self colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            switch (traitCollection.userInterfaceStyle) {
                case UIUserInterfaceStyleDark:
                    return dark;
                case UIUserInterfaceStyleLight:
                case UIUserInterfaceStyleUnspecified:
                default:
                    return light;
            }
        }];
    }else {
        return light;
    }
}

/// 生成颜色
/// - Parameter color: 16进制颜色字符
+ (UIColor *)func_colorWithHexString:(NSString *)color {
    CGFloat r, g, b, a;
    if (func_hexStrToRGBA(color, &r, &g, &b, &a)) {
        return [UIColor colorWithRed:r green:g blue:b alpha:a];
    }
    return nil;
}

/// 生成颜色
/// - Parameter value: rgba
+ (UIColor *)func_rgbColorWithValue:(int)value {
    return  [UIColor colorWithRed:((float)((value & 0xFF0000) >> 16))/255.0 \
                            green:((float)((value & 0xFF00) >> 8))/255.0 \
                             blue:((float)(value & 0xFF))/255.0 \
                            alpha:1.0];
}


static inline NSUInteger func_hexStrToInt(NSString *str) {
    uint32_t result = 0;
    sscanf([str UTF8String], "%x", &result);
    return result;
}

static BOOL func_hexStrToRGBA(NSString *str, CGFloat *r, CGFloat *g, CGFloat *b, CGFloat *a) {
    str = [str uppercaseString];
    
    if ([str hasPrefix:@"#"]) {
        str = [str substringFromIndex:1];
    }else if ([str hasPrefix:@"0X"]) {
        str = [str substringFromIndex:2];
    }else if ([str hasPrefix:@"0x"]) {
        str = [str substringFromIndex:2];
    }
    
    NSUInteger length = [str length];
    if (length != 3 && length != 4 && length != 6 && length != 8) {
        return NO;
    }
    
    if (length < 5) {
        *r = func_hexStrToInt([str substringWithRange:NSMakeRange(0, 1)]) / 255.0f;
        *g = func_hexStrToInt([str substringWithRange:NSMakeRange(1, 1)]) / 255.0f;
        *b = func_hexStrToInt([str substringWithRange:NSMakeRange(2, 1)]) / 255.0f;
        if (length == 4)  *a = func_hexStrToInt([str substringWithRange:NSMakeRange(3, 1)]) / 255.0f;
        else *a = 1;
    }else {
        *r = func_hexStrToInt([str substringWithRange:NSMakeRange(0, 2)]) / 255.0f;
        *g = func_hexStrToInt([str substringWithRange:NSMakeRange(2, 2)]) / 255.0f;
        *b = func_hexStrToInt([str substringWithRange:NSMakeRange(4, 2)]) / 255.0f;
        if (length == 8) *a = func_hexStrToInt([str substringWithRange:NSMakeRange(6, 2)]) / 255.0f;
        else *a = 1;
    }
    return YES;
}

@end
