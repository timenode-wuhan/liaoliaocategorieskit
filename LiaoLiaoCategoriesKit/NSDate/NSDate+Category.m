//
//  CategoriesKit
//  NSDate+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "NSDate+Category.h"

@implementation NSDate (Category)

/**
 * 获取星座
 */
- (NSString *)func_starSignString {
    NSInteger proxy = [self month] * 100 + [self day];
    if (proxy>= 120 && proxy < 219) {
        return NSLocalizedString(@"水瓶座", nil);
    }
    if (proxy >= 219 && proxy < 321) {
        return NSLocalizedString(@"双鱼座", nil);
    }
    if (proxy >= 321 && proxy < 420) {
        return NSLocalizedString(@"白羊座", nil);
    }
    if (proxy >= 420 && proxy < 521) {
        return NSLocalizedString(@"金牛座", nil);
    }
    if (proxy >= 521 && proxy < 622) {
        return NSLocalizedString(@"双子座", nil);
    }
    if (proxy >= 622 && proxy < 723) {
        return NSLocalizedString(@"巨蟹座", nil);
    }
    if (proxy >= 723 && proxy < 823) {
        return NSLocalizedString(@"狮子座", nil);
    }
    if (proxy >= 823 && proxy < 923) {
        return NSLocalizedString(@"处女座", nil);
    }
    if (proxy >= 923 && proxy < 1024) {
        return NSLocalizedString(@"天秤座", nil);
    }
    if (proxy >= 1024 && proxy < 1123) {
        return NSLocalizedString(@"天蝎座", nil);
    }
    if (proxy >= 1123 && proxy < 1222) {
        return NSLocalizedString(@"射手座", nil);
    }
    return NSLocalizedString(@"摩羯座", nil);
}

/**
 * 获取系统时间
 * 返回 xxxx-xx-xx xx:xx:xx
 */
+ (NSString *)func_getSystemTimeString {
    return [NSString stringWithFormat:@"%lld", [self func_getSystemTimeInterval]];
}

+ (long long)func_getSystemTimeInterval {
    long long notification = [[NSDate date] timeIntervalSince1970];
    return notification;
}

/**
 * 根据日期格式，返回时间yyyyddmm
 * format：日期格式（yyyyMMdd:hhmmss）
 */
- (NSString *)func_stringWithFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter stringFromDate:self];
}


- (NSInteger)day {
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:self] day];
}

- (NSInteger)month {
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:self] month];
}


@end
