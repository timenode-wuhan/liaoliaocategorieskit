//
//  CategoriesKit
//  NSDate+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <Foundation/Foundation.h>

@interface NSDate (Category)

/**
 * 获取系统时间
 * 返回 xxxx-xx-xx xx:xx:xx
 */
+ (NSString *)func_getSystemTimeString;

/**
 * 获取星座
 */
- (NSString *)func_starSignString;

/**
 * 根据日期格式，返回时间yyyyddmm
 * format：日期格式（yyyyMMdd:hhmmss）
 */
- (NSString *)func_stringWithFormat:(NSString *)format;

@end
