//
//  CategoriesKit
//  NSString+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Category)

/// 转字典
@property (nonatomic, copy, readonly) NSDictionary *dictionary;

/// URLDecode
@property (nonatomic, copy, readonly) NSString *URLDecodedString;
@property (nonatomic, copy, readonly) NSString *URLEncodedString;

/// 是否是有效字符
/// 需要同时满足! = nil && 属于NSString && 不等于@"" && 不等于 NSNull
-(BOOL)isValid;

/// 是否包含某个字符
/// - Parameter string: string
-(BOOL)func_containsString:(NSString *)string;

/// 是否是全数字
- (BOOL)func_validateAllNumber;

/// 验证身份证号码
- (BOOL)func_accurateVerifyIDCardNumber;

/// 转字典
/// - Parameter jsonString: 待转换的JSONString
- (NSDictionary *)func_dictionaryWithJsonString:(NSString *)jsonString;

/// 获取文本高度
/// - Parameters:
///   - value: 文本内容
///   - font: 字体大小
///   - width: 字体宽度
-(float)func_heightForString:(NSString *)value font:(UIFont*)font andWidth:(float)width;


@end
