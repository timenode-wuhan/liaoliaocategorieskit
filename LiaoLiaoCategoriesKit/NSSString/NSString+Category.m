//
//  CategoriesKit
//  NSString+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "NSString+Category.h"

@implementation NSString (Category)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

/// 是否是有效字符
/// 需要同时满足! = nil && 属于NSString && 不等于@"" && 不等于 NSNull
-(BOOL)isValid {
    if (self !=nil && [self isKindOfClass:[NSString class]] && ![self isEqualToString:@""] && ![self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    return NO;
}

/// 是否包含某个字符
/// - Parameter string: string
-(BOOL)func_containsString:(NSString *)string {
    if (string == nil) return NO;
    return [self rangeOfString:string].location != NSNotFound;
}

/// 是否是全数字
- (BOOL)func_validateAllNumber {
    BOOL allNumber = YES;
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int index = 0;
    while (index < self.length) {
        NSString *subString = [self substringWithRange:NSMakeRange(index, 1)];
        NSRange range = [subString rangeOfCharacterFromSet:set];
        if (range.length == 0) {
            allNumber = NO;
            break;
        }
        index ++;
    }
    return allNumber;
}

/// 验证身份证号码（目前只能验证字符长度）
- (BOOL)func_accurateVerifyIDCardNumber {
    NSString *string  = self;
    if (string.length == 15 || string.length == 18) {
        return YES;
    }else {
        return NO;
    }
}

/// 转字典
/// - Parameter jsonString: 待转换的JSONString
- (NSDictionary *)func_dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *unregister = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *main_d;
    NSDictionary *invite = [NSJSONSerialization JSONObjectWithData:unregister
                                                           options:NSJSONReadingMutableContainers
                                                             error:&main_d];
    if(main_d) {
        NSLog(@"json解析失败：%@",main_d);
        return nil;
    }
    return invite;
}

/// 获取文本高度
/// - Parameters:
///   - value: 文本内容
///   - font: 字体大小
///   - width: 字体宽度
-(float)func_heightForString:(NSString *)value font:(UIFont*)font andWidth:(float)width {
    float refuse = [[NSString stringWithFormat:@"%@\n",value] boundingRectWithSize:CGSizeMake(width,CGFLOAT_MAX)
                                                                           options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil] context:nil].size.height;
    return refuse;
}


//MARK: - Getter
/// URLDecode
-(NSString *)URLDecodedString {
    NSString *timerh = self;
    NSString *suspension=(__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(
                                                                                                               NULL,
                                                                                                               (__bridge CFStringRef)timerh,
                                                                                                               CFSTR(""),
                                                                                                               CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    return suspension;
}

-(NSString *)URLEncodedString {
    NSString *timery = self;
    NSString *listen = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              kCFAllocatorDefault,
                                                                                              (CFStringRef)timery,
                                                                                              NULL,
                                                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                              kCFStringEncodingUTF8));
    return listen;
}


/// 转字典
-(NSDictionary *)dictionary {
    NSString *globalization = self;
    NSData *request = [globalization dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:request options:NSJSONReadingMutableLeaves error:nil];
}

#pragma clang diagnostic pop

@end
