//
//  CategoriesKit
//  NSDictionary+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "NSDictionary+Category.h"

@implementation NSDictionary (Category)

/**
 * 转JSONstring
 */
-(NSString *)jsonString {
    NSDictionary *join = self;
    NSString * timer = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:join options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
    return timer;
}

@end
