//
//  CategoriesKit
//  NSDictionary+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Category)

/// 转JSONstring
@property (nonatomic, copy, readonly) NSString *jsonString;

@end
