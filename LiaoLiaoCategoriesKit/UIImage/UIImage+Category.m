//
//  CategoriesKit
//  UIImage+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIImage+Category.h"
#import <AVFoundation/AVFoundation.h>
#import <Accelerate/Accelerate.h>
#import <CommonCrypto/CommonCryptor.h>

@implementation UIImage (Category)

#pragma mark - 直接根据image name 新建 image 并 设置圆角

+(instancetype)func_circleImageNamed:(NSString *)name {
    return [UIImage func_circleImage:[UIImage imageNamed:name]];
}

+ (instancetype)func_circleImage:(UIImage *)objIma {
    UIGraphicsBeginImageContext(objIma.size);
    CGContextRef colorod = UIGraphicsGetCurrentContext();
    
    CGRect curZ = CGRectMake(0, 0, objIma.size.width, objIma.size.height);
    CGContextAddEllipseInRect(colorod, curZ);
    CGContextClip(colorod);
    [objIma drawInRect:curZ];
    UIImage *mlzImageD = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return mlzImageD;
}

/// 生成图片
/// - Parameters:
///   - light: 亮色模式
///   - dark: 暗黑模式
+ (UIImage *)func_imageWithImageLight:(NSString *)light dark:(NSString *)dark {
    UIImage *senderImage = [UIImage imageNamed:light];
    if (!senderImage) {
        return nil;
    }
    if (@available(iOS 13.0, *)) {
        UIImage *handlerImage= [UIImage imageNamed:dark];
        UITraitCollection *const scaleTraitCollection = [UITraitCollection currentTraitCollection];
        UITraitCollection *const darkUnscaledTraitCollection = [UITraitCollection traitCollectionWithUserInterfaceStyle:UIUserInterfaceStyleDark];
        UITraitCollection *const darkScaledTraitCollection = [UITraitCollection traitCollectionWithTraitsFromCollections:@[scaleTraitCollection, darkUnscaledTraitCollection]];
        UIImage *mlzImageyt = [senderImage imageWithConfiguration:[senderImage.configuration configurationWithTraitCollection:[UITraitCollection traitCollectionWithUserInterfaceStyle:UIUserInterfaceStyleLight]]];
        handlerImage = [handlerImage imageWithConfiguration:[handlerImage.configuration configurationWithTraitCollection:[UITraitCollection traitCollectionWithUserInterfaceStyle:UIUserInterfaceStyleDark]]];
        [mlzImageyt.imageAsset registerImage:handlerImage withTraitCollection:darkScaledTraitCollection];
        return mlzImageyt;
    }else {
        return senderImage;
    }
    return nil;
}


/// 获取视频的第一帧画面
/// - Parameter videoPath: 视频地址
+ (UIImage*)func_getVideoPreViewImageWithPath:(NSString *)videoPath {
    NSURL *yellowL = [NSURL URLWithString:videoPath];
    AVURLAsset *that2 = [[AVURLAsset alloc] initWithURL:yellowL options:nil];
    
    AVAssetImageGenerator *datasource         = [[AVAssetImageGenerator alloc] initWithAsset:that2];
    datasource.appliesPreferredTrackTransform = YES;
    
    CMTime time      = CMTimeMakeWithSeconds(0.0, 600);
    NSError *sweetA   = nil;
    
    CMTime little;
    CGImageRef mlzImageC = [datasource copyCGImageAtTime:time actualTime:&little error:&sweetA];
    UIImage *ringingO     = [[UIImage alloc] initWithCGImage:mlzImageC];
    
    return ringingO;
}

/// 截取视频某一帧的画面
/// - Parameters:
///   - videoURL: 视频地址
///   - time: 时长
+ (UIImage*)func_thumbnailImageForVideo:(NSString *)videoURL atTime:(NSTimeInterval)time {
    NSURL *yellow = [NSURL URLWithString:videoURL];
    
    AVURLAsset *that = [[AVURLAsset alloc] initWithURL:yellow options:nil];
    NSParameterAssert(that);
    AVAssetImageGenerator *proxy =[[AVAssetImageGenerator alloc] initWithAsset:that];
    proxy.appliesPreferredTrackTransform = YES;
    proxy.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
    
    CGImageRef redundant = NULL;
    CFTimeInterval images = time;
    NSError *sign = nil;
    redundant = [proxy copyCGImageAtTime:CMTimeMake(images, 60)actualTime:NULL error:&sign];
    
    if(!redundant) {
        NSLog(@"thumbnailImageGenerationError %@",sign);
    }
    UIImage*secretImage = redundant ? [[UIImage alloc]initWithCGImage: redundant] : nil;
    
    return secretImage;
}

/// 根据颜色生成图片
/// - Parameter color: 颜色值
+ (UIImage *)func_imageWithColor:(UIColor *)color {
    CGRect curu = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(curu.size);
    CGContextRef nowq = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(nowq, [color CGColor]);
    CGContextFillRect(nowq, curu);
    UIImage *contextImageF = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return contextImageF;
}

/// 截屏
+ (UIImage *)func_shotScreen {
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    UIGraphicsBeginImageContext(window.bounds.size);
    [window.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *mlzImageR = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return mlzImageR;
}

/// 截取View
+ (UIImage *)func_shotWithView:(UIView *)view {
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *mlzImagev = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return mlzImagev;
}


/// 截取指定范围的view
/// - Parameters:
///   - view: 被截取的视图
///   - scope: 范围
+ (UIImage *)func_shotWithView:(UIView *)view scope:(CGRect)scope {
    CGImageRef beganS = CGImageCreateWithImageInRect([self func_shotWithView:view].CGImage, scope);
    UIGraphicsBeginImageContext(scope.size);
    CGContextRef nowO = UIGraphicsGetCurrentContext();
    CGRect curt = CGRectMake(0, 0, scope.size.width, scope.size.height);
    CGContextTranslateCTM(nowO, 0, curt.size.height);
    CGContextScaleCTM(nowO, 1.0f, -1.0f);
    CGContextDrawImage(nowO, curt, beganS);
    UIImage *mlzImageu = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    CGImageRelease(beganS);
    CGContextRelease(nowO);
    return mlzImageu;
}


/// 把图片颜色改为灰色
/// - Parameter oldImage: 原图
+(UIImage *)changeGrayImage:(UIImage *)oldImage {
    CIContext *nowG = [CIContext contextWithOptions:nil];
    CIImage *indexpathImage = [CIImage imageWithCGImage:oldImage.CGImage];
    CIFilter *expected = [CIFilter filterWithName:@"CIColorControls"];
    [expected setValue:indexpathImage forKey:kCIInputImageKey];
    [expected setValue:@(0) forKey:@"inputBrightness"];
    [expected setValue:@(0) forKey:@"inputSaturation"];
    [expected setValue:@(0.2) forKey:@"inputContrast"];
    CIImage *pagez = [expected valueForKey:kCIOutputImageKey];
    CGImageRef tmpImageo = [nowG createCGImage:pagez fromRect:[indexpathImage extent]];
    UIImage *dialingImage =  [UIImage imageWithCGImage:tmpImageo];
    CGImageRelease(tmpImageo);
    return dialingImage;
}

@end
