//
//  CategoriesKit
//  UIImage+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)

/// 初始化图片，并切圆角
/// - Parameter objIma: 图片
+ (instancetype)func_circleImage:(UIImage *)objIma;

/// 初始化图片，并切圆角
/// - Parameter objIma: 图片名称
+(instancetype)func_circleImageNamed:(NSString *)name;

/// 生成图片
/// - Parameters:
///   - light: 亮色模式
///   - dark: 暗黑模式
+ (UIImage *)func_imageWithImageLight:(NSString *)light dark:(NSString *)dark;

/// 获取视频的第一帧画面
/// - Parameter videoPath: 视频地址
+ (UIImage*)func_getVideoPreViewImageWithPath:(NSString *)videoPath;

/// 截取视频某一帧的画面
/// - Parameters:
///   - videoURL: 视频地址
///   - time: 时长
+ (UIImage*)func_thumbnailImageForVideo:(NSString *)videoURL atTime:(NSTimeInterval)time;

/// 根据颜色生成图片
/// - Parameter color: 颜色值
+ (UIImage *)func_imageWithColor:(UIColor *)color;

/// 截屏
+ (UIImage *)func_shotScreen;

/// 截取View
+ (UIImage *)func_shotWithView:(UIView *)view;

/// 截取指定范围的view
/// - Parameters:
///   - view: 被截取的视图
///   - scope: 范围
+ (UIImage *)func_shotWithView:(UIView *)view scope:(CGRect)scope;

/// 把图片颜色改为灰色
/// - Parameter oldImage: 原图
+(UIImage *)changeGrayImage:(UIImage *)oldImage;

@end
