//
//  CategoriesKit
//  NSObject+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <Foundation/Foundation.h>

@interface NSObject (Category)

/**
 * 异步执行
 */
- (void)func_performAsynchronous:(void(^)(void))block;

/**
 * 主线程执行
 * wait = YES：同步
 * wait = NO：异步
 */
- (void)func_performOnMainThread:(void(^)(void))block;  // 同步
- (void)func_performOnMainThread:(void(^)(void))block wait:(BOOL)wait;  // 同步&&异步

/**
 * 延迟执行
 */
- (void)func_performAfter:(NSTimeInterval)seconds block:(void(^)(void))block;


@end
