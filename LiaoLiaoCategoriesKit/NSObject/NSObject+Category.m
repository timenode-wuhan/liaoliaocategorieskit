//
//  CategoriesKit
//  NSObject+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "NSObject+Category.h"

@implementation NSObject (Category)

/**
 * 异步执行
 */
- (void)func_performAsynchronous:(void(^)(void))block {
    dispatch_queue_t prosonal = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(prosonal, block);
}

/**
 * 延迟执行
 */
- (void)func_performAfter:(NSTimeInterval)seconds block:(void(^)(void))block {
    dispatch_time_t colorR = dispatch_time(DISPATCH_TIME_NOW, seconds * NSEC_PER_SEC);
    dispatch_after(colorR, dispatch_get_main_queue(), block);
}

/**
 * 主线程执行
 * wait = YES：同步
 * wait = NO：异步
 */
- (void)func_performOnMainThread:(void(^)(void))block {
    [self func_performOnMainThread:block wait:NO];
}

- (void)func_performOnMainThread:(void(^)(void))block wait:(BOOL)shouldWait {
    if ([NSThread isMainThread]) {
        block();
    }else {
        if (shouldWait) {
            dispatch_sync(dispatch_get_main_queue(), block);
        }else {
            dispatch_async(dispatch_get_main_queue(), block);
        }
    }
}


@end
