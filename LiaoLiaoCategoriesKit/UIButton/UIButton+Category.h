//
//  CategoriesKit
//  UIButton+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

typedef void (^TouchedButtonBlock)(UIButton * button);
typedef void (^CloseBlock)(id objc);

/**
 * button 的样式，以图片为基准
 */
typedef NS_ENUM(NSInteger, ButtonLayoutStyle) {
    Normal = 0,       // 内容居中-图左文右
    CenterImageRight, // 内容居中-图右文左
    CenterImageTop,   // 内容居中-图上文下
    CenterImageBottom,// 内容居中-图下文上
    LeftImageLeft,    // 内容居左-图左文右
    LeftImageRight,   // 内容居左-图右文左
    RightImageLeft,   // 内容居右-图左文右
    RightImageRight,  // 内容居右-图右文左
};


@interface UIButton (Category)

@property (nonatomic, copy) CloseBlock closeBlock;
// 图文排布方式
@property (nonatomic, assign) ButtonLayoutStyle layoutType;
// 图文间距，默认为：0
@property (nonatomic, assign) CGFloat padding;
// 图文边界的间距，默认为：5
@property (nonatomic, assign) CGFloat paddingInset;
// 点击范围
@property (nonatomic, assign) UIEdgeInsets clickEdgeInsets;

// 角标配置
@property (strong, nonatomic) UILabel *badgeLabel;
@property (nonatomic) NSString *badgeValue;
@property (nonatomic) UIColor *badgeBgColor;
@property (nonatomic) UIColor *badgeTextColor;
@property (nonatomic) UIFont *badgeFont;
@property (nonatomic) CGFloat badgePadding;
@property (nonatomic) CGFloat badgeMinSize;
@property (nonatomic) CGFloat badgeOriginX;
@property (nonatomic) CGFloat badgeOriginY;

// 等于0时，隐藏角标
@property BOOL shouldHideBadgeAtZero;
// 角标动画
@property BOOL shouldAnimateBadge;

/**
 * 快速构建button
 * - Parameters:
 *   - title: 标题
 *   - backColor: 背景颜色
 *   - backImageName: 背景图片
 *   - titleColor: 标题颜色
 *   - fontSize: 标题大小
 *   - frame: 位置
 *   - cornerRadius: 圆角大小
 */
+(instancetype)func_buttonWithTitle:(NSString *)title backColor:(UIColor *)backColor backImageName:(NSString *)backImageName titleColor:(UIColor *)titleColor fontSize:(int)fontSize frame:(CGRect)frame cornerRadius:(CGFloat)cornerRadius;


#pragma mark - ---------------------- 实例方法在下面 ----------------------

// 点击范围
- (void)setMinEventTouchSize:(CGSize)minSize;

// 添加手势点击
- (void)func_addActionHandler:(TouchedButtonBlock)touchHandler;

// 显示菊花圈
- (void)func_showIndicator;

// 隐藏菊花圈
- (void)func_hideIndicator;

/**
 * 开始倒计时
 * - Parameters:
 *   - timeout: 倒计时
 *   - waitBlock: 正在倒计时回调
 *   - finishBlock: 完成倒计时回调
 */
- (void)startTime:(NSInteger)timeout waitBlock:(void(^)(NSInteger remainTime))waitBlock finishBlock:(void(^)(void))finishBlock;

/**
 * 开始倒计时
 * - Parameters:
 *   - timeLine: 总倒计时时长
 *   - normalTitle: 正常模式下按钮title
 *   - countdownSubtitle: 倒计时状态下按钮title
 *   - normalColor: 正常模式下按钮颜色
 *   - countdownColor: 倒计时状态下按钮颜色
 *   - isFlashing: 是否闪烁
 *   - finishBlock: 完成倒计时回调
 */
- (void)func_countdownTime:(NSInteger)timeLine normalTitle:(NSString *)normalTitle countdownSubtitle:(NSString *)countdownSubtitle normalColor:(UIColor *)normalColor countdownColor:(UIColor *)countdownColor isFlashing:(BOOL)isFlashing finishBlock:(void(^)(void))finishBlock;


@end
