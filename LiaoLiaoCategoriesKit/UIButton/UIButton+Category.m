//
//  CategoriesKit
//  UIButton+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIButton+Category.h"
#import <objc/runtime.h>

static NSString * const kbuttonContentLayoutTypeKey = @"axcUI_buttonContentLayoutTypeKey";
static NSString * const kpaddingKey = @"paddingKey";
static NSString * const kpaddingInsetKey = @"paddingInsetKey";
static NSString * const IndicatorViewKey = @"indicatorView";
static NSString * const ButtonTextObjectKey = @"buttonTextObject";
static NSString * const UIButton_badgeKey = @"UIButton_badgeKey";
static NSString * const UIButton_closeBlockKey = @"UIButton_closeBlockKey";
static NSString * const UIButton_badgeBGColorKey = @"UIButton_badgeBGColorKey";
static NSString * const UIButton_badgeTextColorKey = @"UIButton_badgeTextColorKey";
static NSString * const UIButton_badgeFontKey = @"UIButton_badgeFontKey";
static NSString * const UIButton_badgePaddingKey = @"UIButton_badgePaddingKey";
static NSString * const UIButton_badgeMinSizeKey = @"UIButton_badgeMinSizeKey";
static NSString * const UIButton_badgeOriginXKey = @"UIButton_badgeOriginXKey";
static NSString * const UIButton_badgeOriginYKey = @"UIButton_badgeOriginYKey";
static NSString * const UIButton_shouldHideBadgeAtZeroKey = @"UIButton_shouldHideBadgeAtZeroKey";
static NSString * const UIButton_shouldAnimateBadgeKey = @"UIButton_shouldAnimateBadgeKey";
static NSString * const UIButton_badgeValueKey = @"UIButton_badgeValueKey";

@interface UIButton ()

@property (nonatomic, assign) CGSize eventFrame;

@end

@implementation UIButton (Category)

@dynamic badgeValue, badgeBgColor, badgeTextColor, badgeFont;
@dynamic badgePadding, badgeMinSize, badgeOriginX, badgeOriginY;
@dynamic shouldHideBadgeAtZero, shouldAnimateBadge;

/**
 * 快速构建button
 * - Parameters:
 *   - title: 标题
 *   - backColor: 背景颜色
 *   - backImageName: 背景图片
 *   - titleColor: 标题颜色
 *   - fontSize: 标题大小
 *   - frame: 位置
 *   - cornerRadius: 圆角大小
 */
+(instancetype)func_buttonWithTitle:(NSString *)title backColor:(UIColor *)backColor backImageName:(NSString *)backImageName titleColor:(UIColor *)titleColor fontSize:(int)fontSize frame:(CGRect)frame cornerRadius:(CGFloat)cornerRadius {
    UIButton *assistButton = [UIButton new];
    [assistButton setTitle:title forState:UIControlStateNormal];
    [assistButton setBackgroundColor:backColor];
    [assistButton setBackgroundImage:[UIImage imageNamed:backImageName] forState:UIControlStateNormal];
    [assistButton setTitleColor:titleColor forState:UIControlStateNormal];
    assistButton.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [assistButton sizeToFit];
    assistButton.frame = frame;
    assistButton.layer.cornerRadius = cornerRadius;
    assistButton.clipsToBounds = YES;
    return assistButton;
}

/**
 * 初始化角标
 */
- (void)badgeInit {
    self.badgeBGColor   = [UIColor redColor];
    self.badgeTextColor = [UIColor whiteColor];
    self.badgeFont      = [UIFont systemFontOfSize:9.0];
    self.badgePadding   = 5;
    self.badgeMinSize   = 4;
    self.badgeOriginX   = self.frame.size.width - self.badgeLabel.frame.size.width/2-3;
    self.badgeOriginY   = -6;
    self.shouldHideBadgeAtZero = YES;
    self.shouldAnimateBadge = YES;
    self.clipsToBounds = NO;
}

/**
 * 添加点击手势
 */
- (void)func_addActionHandler:(TouchedButtonBlock)touchHandler {
    objc_setAssociatedObject(self, @selector(func_addActionHandler:), touchHandler, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(blockActionTouched:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark - SET

/**
 * 设置角标最小size
 */
-(void)setBadgeMinSize:(CGFloat)badgeMinSize {
    NSNumber *swift = [NSNumber numberWithDouble:badgeMinSize];
    objc_setAssociatedObject(self, &UIButton_badgeMinSizeKey, swift, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self updateBadgeFrame];
    }
}

/**
 * 设置角标文字大小
 */
-(void)setBadgeFont:(UIFont *)badgeFont {
    objc_setAssociatedObject(self, &UIButton_badgeFontKey, badgeFont, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self refreshBadge];
    }
}

/**
 * 更新角标
 * - Parameter animated: 是否需要动画
 */
- (void)updateBadgeValueAnimated:(BOOL)animated {
    if (animated && self.shouldAnimateBadge && ![self.badgeLabel.text isEqualToString:self.badgeValue]) {
        CABasicAnimation * esult = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [esult setFromValue:[NSNumber numberWithFloat:1.5]];
        [esult setToValue:[NSNumber numberWithFloat:1]];
        [esult setDuration:0.2];
        [esult setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4f :1.3f :1.f :1.f]];
        [self.badgeLabel.layer addAnimation:esult forKey:@"bounceAnimation"];
    }
    
    self.badgeLabel.text = self.badgeValue;
    
    NSTimeInterval income = animated ? 0.2 : 0;
    [UIView animateWithDuration:income animations:^{
        [self updateBadgeFrame];
    }];
}

/**
 * 清除角标
 */
- (void)removeBadge {
    [UIView animateWithDuration:0.2 animations:^{
        self.badgeLabel.transform = CGAffineTransformMakeScale(0, 0);
    } completion:^(BOOL finished) {
        [self.badgeLabel removeFromSuperview];
        self.badgeLabel = nil;
    }];
}

/**
 * 设置角标X
 */
-(void)setBadgeOriginX:(CGFloat)badgeOriginX {
    NSNumber *swiftH = [NSNumber numberWithDouble:badgeOriginX];
    objc_setAssociatedObject(self, &UIButton_badgeOriginXKey, swiftH, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self updateBadgeFrame];
    }
}

/**
 * 设置角标数值
 */
-(void)setBadgeValue:(NSString *)badgeValue {
    objc_setAssociatedObject(self, &UIButton_badgeValueKey, badgeValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (badgeValue.integerValue == 0) {
        [self removeBadge];
    }else if (!self.badgeLabel) {
        self.badgeLabel                          = [[UILabel alloc] initWithFrame:CGRectMake(self.badgeOriginX, self.badgeOriginY, 20, 20)];
        self.badgeLabel.textColor             = self.badgeTextColor;
        self.badgeLabel.backgroundColor  = self.badgeBGColor;
        self.badgeLabel.font                    = self.badgeFont;
        self.badgeLabel.textAlignment       = NSTextAlignmentCenter;
        [self badgeInit];
        [self addSubview:self.badgeLabel];
        [self updateBadgeValueAnimated:NO];
    }else {
        [self updateBadgeValueAnimated:YES];
    }
}

/**
 * 更新角标位置
 */
- (void)updateBadgeFrame {
    CGSize circle = [self badgeExpectedSize];
    CGFloat contra = circle.height;
    
    contra = (contra < self.badgeMinSize) ? self.badgeMinSize : circle.height;
    CGFloat drop = circle.width;
    CGFloat methods = self.badgePadding;
    
    drop = (drop < contra) ? contra : circle.width;
    self.badgeLabel.frame = CGRectMake(self.badgeOriginX, self.badgeOriginY, drop + methods, contra + methods);
    self.badgeLabel.layer.cornerRadius = (contra + methods) / 2;

    self.badgeLabel.layer.masksToBounds = YES;
}

/**
 * 设置最小点击区域
 */
- (void)setMinEventTouchSize:(CGSize)minSize {
    self.eventFrame = minSize;
}

/**
 * 设置点击区域
 */
- (void)setEventFrame:(CGSize)eventFrame {
    NSString *teal = NSStringFromCGSize(eventFrame);
    objc_setAssociatedObject(self, _cmd, teal, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

/**
 * 设置内容偏移
 */
- (void)setPaddingInset:(CGFloat)paddingInset {
    [self willChangeValueForKey:kpaddingInsetKey];
    objc_setAssociatedObject(self, &kpaddingInsetKey, @(paddingInset),  OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self didChangeValueForKey:kpaddingInsetKey];
    [self setupButtonLayout];
}

- (void)blockActionTouched:(UIButton *)btn {
    TouchedButtonBlock callx = objc_getAssociatedObject(self, @selector(func_addActionHandler:));
    if (callx) {
        callx(btn);
    }
}

/**
 * 重写点击方法
 */
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (UIEdgeInsetsEqualToEdgeInsets(self.clickEdgeInsets, UIEdgeInsetsZero)) {
        return [super pointInside:point withEvent:event];
    }else {
        CGRect dimensional = UIEdgeInsetsInsetRect(self.bounds, self.clickEdgeInsets);
        return CGRectContainsPoint(dimensional, point) ? YES : NO;
    }
}

-(CloseBlock)closeBlock {
    return objc_getAssociatedObject(self, &UIButton_closeBlockKey);
}

/**
 * 设置图文排版模式
 */
- (void)setLayoutType:(ButtonLayoutStyle)layoutType {
    [self willChangeValueForKey:kbuttonContentLayoutTypeKey];
    objc_setAssociatedObject(self, &kbuttonContentLayoutTypeKey, @(layoutType), OBJC_ASSOCIATION_ASSIGN);
    [self didChangeValueForKey:kbuttonContentLayoutTypeKey];
    [self setupButtonLayout];
}

/**
 * 开始倒计时
 * - Parameters:
 *   - timeout: 倒计时
 *   - waitBlock: 正在倒计时回调
 *   - finishBlock: 完成倒计时回调
 */
- (void)startTime:(NSInteger)timeout waitBlock:(void(^)(NSInteger remainTime))waitBlock finishBlock:(void(^)(void))finishBlock {
    __block NSInteger timeOut = timeout;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(timeOut <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finishBlock) {
                    finishBlock();
                }
                self.userInteractionEnabled = YES;
            });
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (waitBlock) {
                    waitBlock(timeOut);
                }
                self.userInteractionEnabled = NO;
            });
            timeOut--;
        }
    });
    dispatch_resume(_timer);
}

- (void)setShouldAnimateBadge:(BOOL)shouldAnimateBadge {
    NSNumber *swiftT = [NSNumber numberWithBool:shouldAnimateBadge];
    objc_setAssociatedObject(self, &UIButton_shouldAnimateBadgeKey, swiftT, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(void)setBadgePadding:(CGFloat)badgePadding {
    NSNumber *swifte = [NSNumber numberWithDouble:badgePadding];
    objc_setAssociatedObject(self, &UIButton_badgePaddingKey, swifte, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self updateBadgeFrame];
    }
}


-(void)setBadgeOriginY:(CGFloat)badgeOriginY {
    NSNumber *swiftD = [NSNumber numberWithDouble:badgeOriginY];
    objc_setAssociatedObject(self, &UIButton_badgeOriginYKey, swiftD, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self updateBadgeFrame];
    }
}


/**
 * 隐藏菊花圈
 */
- (void)func_hideIndicator {
    NSString *delegae = (NSString *)objc_getAssociatedObject(self, &ButtonTextObjectKey);
    UIActivityIndicatorView *chat = (UIActivityIndicatorView *)objc_getAssociatedObject(self, &IndicatorViewKey);
    
    self.enabled = YES;
    [chat removeFromSuperview];
    [self setTitle:delegae forState:UIControlStateNormal];
}

/**
 * 显示菊花圈
 */
- (void)func_showIndicator {
    UIActivityIndicatorView *chat6 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    chat6.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    [chat6 startAnimating];
    
    NSString *delegaea = self.titleLabel.text;
    objc_setAssociatedObject(self, &ButtonTextObjectKey, delegaea, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(self, &IndicatorViewKey, chat6, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    self.enabled = NO;
    [self setTitle:@"" forState:UIControlStateNormal];
    [self addSubview:chat6];
}

/**
 * 设置角标文字颜色
 */
-(void)setBadgeTextColor:(UIColor *)badgeTextColor {
    objc_setAssociatedObject(self, &UIButton_badgeTextColorKey, badgeTextColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self refreshBadge];
    }
}

/**
 * 是否在角标数值等0时，隐藏角标
 */
- (void)setShouldHideBadgeAtZero:(BOOL)shouldHideBadgeAtZero {
    NSNumber *swiftJ = [NSNumber numberWithBool:shouldHideBadgeAtZero];
    objc_setAssociatedObject(self, &UIButton_shouldHideBadgeAtZeroKey, swiftJ, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 * 点击block
 */
-(void)setCloseBlock:(CloseBlock )closeBlock {
    objc_setAssociatedObject(self, &UIButton_closeBlockKey, closeBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

/**
 * 更新角标
 */
- (void)refreshBadge {
    self.badgeLabel.textColor        = self.badgeTextColor;
    self.badgeLabel.backgroundColor  = self.badgeBGColor;
    self.badgeLabel.font             = self.badgeFont;
}

/**
 * 设置点击偏移区域
 */
- (void)setClickEdgeInsets:(UIEdgeInsets)clickEdgeInsets {
    objc_setAssociatedObject(self, @selector(clickEdgeInsets), [NSValue valueWithUIEdgeInsets:clickEdgeInsets], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 * 设置角标背景颜色
 */
-(void)setBadgeBGColor:(UIColor *)badgeBGColor {
    objc_setAssociatedObject(self, &UIButton_badgeBGColorKey, badgeBGColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.badgeLabel) {
        [self refreshBadge];
    }
}

/**
 * 设置角标label
 */
- (void)setBadgeLabel:(UILabel *)badgeLabel {
    objc_setAssociatedObject(self, &UIButton_badgeKey, badgeLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 * 设置偏移
 */
- (void)setPadding:(CGFloat)padding {
    [self willChangeValueForKey:kpaddingKey];
    objc_setAssociatedObject(self, &kpaddingKey, @(padding), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self didChangeValueForKey:kpaddingKey];
    
    [self setupButtonLayout];
}


/**
 * 开始倒计时
 * - Parameters:
 *   - timeout: 倒计时
 *   - waitBlock: 正在倒计时回调
 *   - finishBlock: 完成倒计时回调
 */
- (void)func_countdownTime:(NSInteger)timeLine normalTitle:(NSString *)normalTitle countdownSubtitle:(NSString *)countdownSubtitle normalColor:(UIColor *)normalColor countdownColor:(UIColor *)countdownColor isFlashing:(BOOL)isFlashing finishBlock:(void(^)(void))finishBlock{
    [self func_countdownTime:timeLine normalTitle:normalTitle countNumBeforeTitle:nil countNumBehindTitle:countdownSubtitle normalColor:normalColor countdownColor:countdownColor isFlashing:isFlashing cuntingEnabled:NO waitBlock:^(NSInteger remainTime) {
        // remainTime
    } finishBlock:finishBlock];
}

/**
 * 开始倒计时
 * - Parameters:
 *   - timeLine: 总倒计时时长
 *   - normalTitle: 正常模式下按钮title
 *   - countdownSubtitle: 倒计时状态下按钮title
 *   - normalColor: 正常模式下按钮颜色
 *   - countdownColor: 倒计时状态下按钮颜色
 *   - isFlashing: 是否闪烁
 *   - finishBlock: 完成倒计时回调
 */
- (void)func_countdownTime:(NSInteger)timeLine normalTitle:(NSString *)normalTitle countNumBeforeTitle:(NSString *)beforeTitle countNumBehindTitle:(NSString *)behindTitle normalColor:(UIColor *)normalColor countdownColor:(UIColor *)countdownColor isFlashing:(BOOL)isFlashing cuntingEnabled:(BOOL)cuntingEnabled waitBlock:(void(^)(NSInteger remainTime))waitBlock finishBlock:(void(^)(void))finishBlock {
    
    __weak __typeof(self)weakSelf = self;
    
    __block NSInteger label = timeLine;
    dispatch_queue_t prosonal = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_source_t parameters = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,0, 0, prosonal);
    
    dispatch_source_set_timer(parameters,dispatch_walltime(NULL,0), 1.0 * NSEC_PER_SEC,0);
    dispatch_source_set_event_handler(parameters, ^{
        
        
        if (label == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (normalColor) {
                    [self setTitleColor:normalColor forState:UIControlStateNormal];
                }
                [self setTitle:normalTitle forState:UIControlStateNormal];
                self.userInteractionEnabled =YES;
                dispatch_source_cancel(parameters);
                
                if (finishBlock) {
                    finishBlock();
                }
            });
        }else {
            NSInteger volume = label / (60*60);
            NSInteger codec = (label - (volume *3600)) / 60;
            NSInteger set5 = ((label == timeLine) && label<=60) ? label: (label % 60);
            NSString *m_count = [NSString stringWithFormat:@"%02ld", set5];
            if (volume) {
                m_count = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", volume,codec,set5];
            }else {
                if (codec) {
                    m_count = [NSString stringWithFormat:@"%02ld:%02ld", codec,set5];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (countdownColor) {
                    [self setTitleColor:countdownColor forState:UIControlStateNormal];
                }
                NSString *wrapper = [NSString stringWithFormat:@"%@%@%@",(beforeTitle.length ? beforeTitle : @""),m_count,(behindTitle.length ? behindTitle : @"")];
                if (!isFlashing) {
                    self.titleLabel.text = wrapper ;
                }
                [self setTitle:wrapper forState:UIControlStateNormal];
                
                if (cuntingEnabled) {
                    self.userInteractionEnabled = YES;
                }else{
                    self.userInteractionEnabled =NO;
                }
            });
            label--;
            if (waitBlock && label) {
                waitBlock(label);
            }
            
            weakSelf.closeBlock = ^(id objc) {
                dispatch_source_cancel(parameters);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (normalColor){
                        [weakSelf setTitleColor:normalColor forState:UIControlStateNormal];
                    }
                    [weakSelf setTitle:normalTitle forState:UIControlStateNormal];
                    weakSelf.userInteractionEnabled =YES;
                });
            };
        }
    });
    dispatch_resume(parameters);
}

//MARK: - Setter
- (void)setupButtonLayout {
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    CGFloat supports = self.imageView.frame.size.width;
    CGFloat countdown = self.imageView.frame.size.height;
    
    CGFloat light = self.titleLabel.frame.size.width;
    CGFloat arrow = self.titleLabel.frame.size.height;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        
        light = self.titleLabel.intrinsicContentSize.width;
        arrow = self.titleLabel.intrinsicContentSize.height;
    }
    
    UIEdgeInsets nlarge = UIEdgeInsetsZero;
    UIEdgeInsets paths = UIEdgeInsetsZero;
    
    if (self.paddingInset == 0){
        self.paddingInset = 5;
    }
    
    switch (self.layoutType) {
        case Normal:{
            paths = UIEdgeInsetsMake(0, self.padding, 0, 0);
            nlarge = UIEdgeInsetsMake(0, 0, 0, self.padding);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }
            break;
        case CenterImageRight:{
            paths = UIEdgeInsetsMake(0, -supports - self.padding, 0, supports);
            nlarge = UIEdgeInsetsMake(0, light + self.padding, 0, -light);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }
            break;
        case CenterImageTop:{
            paths = UIEdgeInsetsMake(0, -supports, -countdown - self.padding, 0);
            nlarge = UIEdgeInsetsMake(-arrow - self.padding, 0, 0, -light);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }
            break;
        case CenterImageBottom:{
            paths = UIEdgeInsetsMake(-countdown - self.padding, -supports, 0, 0);
            nlarge = UIEdgeInsetsMake(0, 0, -arrow - self.padding, -light);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }
            break;
        case LeftImageLeft:{
            paths = UIEdgeInsetsMake(0, self.padding + self.paddingInset, 0, 0);
            nlarge = UIEdgeInsetsMake(0, self.paddingInset, 0, 0);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
            break;
        case LeftImageRight:{
            paths = UIEdgeInsetsMake(0, -supports + self.paddingInset, 0, 0);
            nlarge = UIEdgeInsetsMake(0, light + self.padding + self.paddingInset, 0, 0);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
            break;
        case RightImageLeft:{
            nlarge = UIEdgeInsetsMake(0, 0, 0, self.padding + self.paddingInset);
            paths = UIEdgeInsetsMake(0, 0, 0, self.paddingInset);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
            break;
        case RightImageRight:{
            paths = UIEdgeInsetsMake(0, -self.frame.size.width / 2, 0, supports + self.padding + self.paddingInset);
            nlarge = UIEdgeInsetsMake(0, 0, 0, -light + self.paddingInset);
            self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
            break;
        default:break;
    }
    self.imageEdgeInsets = nlarge;
    self.titleEdgeInsets = paths;
    [self setNeedsDisplay];
}

/// 角标Label
-(UILabel*)badgeLabel {
    return objc_getAssociatedObject(self, &UIButton_badgeKey);
}

/**
 * 角标背景颜色
 */
-(UIColor *)badgeBGColor {
    return objc_getAssociatedObject(self, &UIButton_badgeBGColorKey);
}

/**
 * 角标内容
 */
-(NSString *)badgeValue {
    return objc_getAssociatedObject(self, &UIButton_badgeValueKey);
}

/**
 * 角标文字颜色
 */
-(UIColor *)badgeTextColor {
    return objc_getAssociatedObject(self, &UIButton_badgeTextColorKey);
}

/**
 * 角标文字大小
 */
-(UIFont *)badgeFont {
    return objc_getAssociatedObject(self, &UIButton_badgeFontKey);
}

/**
 * 图文布局方式
 */
- (ButtonLayoutStyle)layoutType {
    return [objc_getAssociatedObject(self, &kbuttonContentLayoutTypeKey) integerValue];
}

/**
 * 是否在角标数值为0时，隐藏角标
 */
-(BOOL)shouldHideBadgeAtZero {
    NSNumber *number = objc_getAssociatedObject(self, &UIButton_shouldHideBadgeAtZeroKey);
    return number.boolValue;
}

/**
 * 图文偏移量
 */
- (CGFloat)padding {
    return [objc_getAssociatedObject(self, &kpaddingKey) floatValue];
}

/**
 * 角标最小大小
 */
-(CGFloat)badgeMinSize {
    NSNumber *swiftM = objc_getAssociatedObject(self, &UIButton_badgeMinSizeKey);
    return swiftM.floatValue;
}

/**
 * 角标便宜量
 */
-(CGFloat)badgePadding {
    NSNumber *number = objc_getAssociatedObject(self, &UIButton_badgePaddingKey);
    return number.floatValue;
}

/**
 * 创建角标label
 */
- (UILabel *)duplicateLabel:(UILabel *)labelToCopy {
    UILabel *productLabel = [[UILabel alloc] initWithFrame:labelToCopy.frame];
    productLabel.text = labelToCopy.text;
    productLabel.font = labelToCopy.font;
    
    return productLabel;
}

/**
 * 角标x
 */
-(CGFloat)badgeOriginX {
    NSNumber *swiftN = objc_getAssociatedObject(self, &UIButton_badgeOriginXKey);
    return swiftN.floatValue;
}

/**
 * 角标y
 */
-(CGFloat)badgeOriginY {
    NSNumber *swiftU = objc_getAssociatedObject(self, &UIButton_badgeOriginYKey);
    return swiftU.floatValue;
}

/**
 * 按钮响应点击范围
 */
- (CGSize)eventFrame {
    NSString *teal6 = objc_getAssociatedObject(self, @selector(setEventFrame:));
    return CGSizeFromString(teal6);
}

/**
 * 内偏移
 */
-(CGFloat)paddingInset {
    return [objc_getAssociatedObject(self, &kpaddingInsetKey) floatValue];
}

/**
 * 是否动画显示角标
 */
-(BOOL)shouldAnimateBadge {
    NSNumber *swiftu = objc_getAssociatedObject(self, &UIButton_shouldAnimateBadgeKey);
    return swiftu.boolValue;
}

/**
 * 点击偏移区域
 */
- (UIEdgeInsets)clickEdgeInsets {
    return [objc_getAssociatedObject(self, @selector(clickEdgeInsets)) UIEdgeInsetsValue];
}

/**
 * 角标大小
 */
- (CGSize)badgeExpectedSize {
    UILabel *etworkingLabel = [self duplicateLabel:self.badgeLabel];
    [etworkingLabel sizeToFit];
    
    CGSize circleM = etworkingLabel.frame.size;
    return circleM;
}

@end
