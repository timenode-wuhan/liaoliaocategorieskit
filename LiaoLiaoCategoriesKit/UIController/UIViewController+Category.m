//
//  CategoriesKit
//  UIViewController+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIViewController+Category.h"

@implementation UIViewController (Category)

/// 加载storyboard的控制器
/// - Parameters:
///   - storyboardName: storyboardName
///   - identifier: identifier
+ (id)func_loadFromStoryboardName:(NSString *)storyboardName identifier:(NSString *)identifier {
    UIStoryboard *contrain = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];
    UIViewController *writerController =  [contrain instantiateViewControllerWithIdentifier:identifier];
    if (writerController) {
        return writerController;
    }
    return UIViewController.new;
}

/**
 * 获取当前的控制器
 */
+ (UIViewController *)func_getCurrentViewController {
    UIViewController *elementsController = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *playController = [self func_getCurrentVCFrom:elementsController];
    return playController;
}

+ (UIViewController *)func_getCurrentVCFrom:(UIViewController *)rootVC {
    UIViewController *playControllerR;
    if([rootVC presentedViewController]) {
        rootVC = [rootVC presentedViewController];
    }
    if([rootVC isKindOfClass:[UITabBarController class]]) {
        playControllerR = [self func_getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
    }else if ([rootVC isKindOfClass:[UINavigationController class]]) {
        playControllerR = [self func_getCurrentVCFrom:[(UINavigationController *)rootVC viewControllers].firstObject];
    }else {
        playControllerR = rootVC;
    }
    return playControllerR;
}


/// 当前控制器是否可见
/// - Parameter viewController: 当前控制器
+(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController {
    return (viewController.isViewLoaded && viewController.view.window);
}



@end
