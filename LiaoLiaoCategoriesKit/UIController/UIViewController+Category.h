//
//  CategoriesKit
//  UIViewController+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Category)

/**
 * 获取当前的控制器
 */
+ (UIViewController *)func_getCurrentViewController;

/// 加载storyboard的控制器
/// - Parameters:
///   - storyboardName: storyboardName
///   - identifier: identifier
+ (id)func_loadFromStoryboardName:(NSString *)storyboardName identifier:(NSString *)identifier;

/// 当前控制器是否可见
/// - Parameter viewController: 当前控制器
+(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController;

/**
 * 获取当前的控制器
 */
+ (UIViewController *)func_getCurrentVCFrom:(UIViewController *)rootVC;

@end

NS_ASSUME_NONNULL_END
