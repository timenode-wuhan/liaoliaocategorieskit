//
//  CategoriesKit
//  UIScrollView+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

/**
 * TableView刷新结果
 */
typedef NS_ENUM(NSInteger, RefreshStatus)  {
    HeaderMoreData = 1,
    HeaderNoMoreData,
    FooterMoreData,
    FooterNoMoreData,
    RefreshError,
};

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (Refresh)

/// 改变刷新状态
/// - Parameter refreshStatus: 刷新状态
-(void)func_refreashWIthStatus:(RefreshStatus)refreshStatus;

@end

NS_ASSUME_NONNULL_END
