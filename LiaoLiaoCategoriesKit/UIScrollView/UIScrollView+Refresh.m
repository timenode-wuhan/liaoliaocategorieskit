//
//  CategoriesKit
//  UIScrollView+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIScrollView+Refresh.h"
#import <MJRefresh/MJRefresh.h>

@implementation UIScrollView (Refresh)

/// 改变刷新状态
/// - Parameter refreshStatus: 刷新状态
-(void)func_refreashWIthStatus:(RefreshStatus)refreshStatus {
    [self.mj_header isRefreshing] ? [self.mj_header endRefreshing] : [self.mj_footer endRefreshing];
    
    switch (refreshStatus) {
        case HeaderNoMoreData: {
            self.mj_footer.hidden = YES;
        }
            break;
        case HeaderMoreData: {
            self.mj_footer.hidden = NO;
            [self.mj_footer resetNoMoreData];
        }
            break;
        case FooterNoMoreData: {
            [self.mj_footer endRefreshingWithNoMoreData];
        }
            break;
        case FooterMoreData: {
            self.mj_footer.hidden = NO;
            [self.mj_footer resetNoMoreData];
        }
            break;
        case RefreshError: {
            self.mj_footer.hidden = YES;
            [self.mj_header endRefreshing];
        }
            break;
            
        default:
            break;
    }
    
}


@end
