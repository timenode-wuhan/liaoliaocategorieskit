//
//  CategoriesKit
//  UIView+Category.m
//
//  Created by 清风徐来 on 2022/7/28.
//

#import "UIView+Category.h"
#import <objc/runtime.h>

static char kActionHandlerTapBlockKey;
static char kActionHandlerTapGestureKey;
static char kActionHandlerLongPressBlockKey;
static char kActionHandlerLongPressGestureKey;

@implementation UIView (Category)
@dynamic clipRadius;
@dynamic radius;
@dynamic borderWidth;
@dynamic borderColor;
@dynamic shadowRadius;
@dynamic shadowColor;
@dynamic shadowOpacity;
@dynamic shadowOffset;

/// 截图
- (UIImage *)func_snapshotImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *star = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return star;
}

/// 截图生成PDF
- (NSData *)func_snapshotPDF {
    CGRect before = self.bounds;
    NSMutableData* free = [NSMutableData data];
    CGDataConsumerRef keep = CGDataConsumerCreateWithCFData((__bridge CFMutableDataRef)free);
    CGContextRef now = CGPDFContextCreate(keep, &before, NULL);
    CGDataConsumerRelease(keep);
    if (!now) return nil;
    CGPDFContextBeginPage(now, NULL);
    CGContextTranslateCTM(now, 0, before.size.height);
    CGContextScaleCTM(now, 1.0, -1.0);
    
    [self.layer renderInContext:now];
    CGPDFContextEndPage(now);
    CGPDFContextClose(now);
    CGContextRelease(now);
    return free;
}

/// 截图
/// - Parameter afterUpdates: 是否等待view更新完成
- (UIImage *)func_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates {
    if (![self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        return [self func_snapshotImage];
    }
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:afterUpdates];
    UIImage *starb = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return starb;
}

/// 添加阴影
/// - Parameters:
///   - shadowColor: 阴影颜色
///   - shadowOpacity: 阴影透明度
///   - shadowRadius: 阴影半径
///   - shadowPathType: 阴影类型
///   - shadowPathWidth: 阴影宽度
- (void)func_viewShadowPathWithColor:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowPathType:(ShadowPathType)shadowPathType shadowPathWidth:(CGFloat)shadowPathWidth {
    
    self.layer.masksToBounds = NO;//必须要等于NO否则会把阴影切割隐藏掉
    self.layer.shadowColor = shadowColor.CGColor;// 阴影颜色
    self.layer.shadowOpacity = shadowOpacity;// 阴影透明度，默认0
    self.layer.shadowOffset = CGSizeZero;//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    self.layer.shadowRadius = shadowRadius;//阴影半径，默认3
    CGRect shadowRect = CGRectZero;
    CGFloat originX,originY,sizeWith,sizeHeight;
    originX = 0;
    originY = 0;
    sizeWith = self.bounds.size.width;
    sizeHeight = self.bounds.size.height;
    
    if (shadowPathType == ShadowPathTop) {
        shadowRect = CGRectMake(originX, originY-shadowPathWidth/2, sizeWith, shadowPathWidth);
    }else if (shadowPathType == ShadowPathBottom) {
        shadowRect = CGRectMake(originY, sizeHeight-shadowPathWidth/2, sizeWith, shadowPathWidth);
    }else if (shadowPathType == ShadowPathLeft) {
        shadowRect = CGRectMake(originX-shadowPathWidth/2, originY, shadowPathWidth, sizeHeight);
    }else if (shadowPathType == ShadowPathRight) {
        shadowRect = CGRectMake(sizeWith-shadowPathWidth/2, originY, shadowPathWidth, sizeHeight);
    }else if (shadowPathType == ShadowPathCommon) {
        shadowRect = CGRectMake(originX-shadowPathWidth/2, 2, sizeWith+shadowPathWidth, sizeHeight+shadowPathWidth/2);
    }else if (shadowPathType == ShadowPathAround) {
        shadowRect = CGRectMake(originX-shadowPathWidth/2, originY-shadowPathWidth/2, sizeWith+shadowPathWidth, sizeHeight+shadowPathWidth);
    }
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRect:shadowRect];
    self.layer.shadowPath = bezierPath.CGPath;//阴影路径
}

/// 指定区域透明（抠图）
/// - Parameter rect: 指定区域
-(void)func_setAlphaComponent:(CGRect)rect {
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.frame];
    [path appendPath:[[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:0] bezierPathByReversingPath]];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    [self.layer setMask:shapeLayer];
}


-(CornerRadiiType)cornerRadiiNull {
    return (CornerRadiiType){-1, -1, -1, -1};
}


/// view转换
- (CGPoint)func_convertPoint:(CGPoint)point toViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *)self) convertPoint:point toWindow:nil];
        } else {
            return [self convertPoint:point toView:nil];
        }
    }
    
    UIWindow *globalization = [self isKindOfClass:[UIWindow class]] ? (id)self : self.window;
    UIWindow *card = [view isKindOfClass:[UIWindow class]] ? (id)view : view.window;
    if ((!globalization || !card) || (globalization == card)) return [self convertPoint:point toView:view];
    point = [self convertPoint:point toView:globalization];
    point = [card convertPoint:point fromWindow:globalization];
    point = [view convertPoint:point fromView:card];
    return point;
}

- (CGPoint)func_convertPoint:(CGPoint)point fromViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *)self) convertPoint:point fromWindow:nil];
        }else {
            return [self convertPoint:point fromView:nil];
        }
    }
    
    UIWindow *globalizationS = [view isKindOfClass:[UIWindow class]] ? (id)view : view.window;
    UIWindow *cardD = [self isKindOfClass:[UIWindow class]] ? (id)self : self.window;
    if ((!globalizationS || !cardD) || (globalizationS == cardD)) return [self convertPoint:point fromView:view];
    point = [globalizationS convertPoint:point fromView:view];
    point = [cardD convertPoint:point fromWindow:globalizationS];
    point = [self convertPoint:point fromView:cardD];
    return point;
}

- (CGRect)func_convertRect:(CGRect)rect toViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *)self) convertRect:rect toWindow:nil];
        }else {
            return [self convertRect:rect toView:nil];
        }
    }
    
    UIWindow *globalization_ = [self isKindOfClass:[UIWindow class]] ? (id)self : self.window;
    UIWindow *cardZ = [view isKindOfClass:[UIWindow class]] ? (id)view : view.window;
    if (!globalization_ || !cardZ) return [self convertRect:rect toView:view];
    if (globalization_ == cardZ) return [self convertRect:rect toView:view];
    rect = [self convertRect:rect toView:globalization_];
    rect = [cardZ convertRect:rect fromWindow:globalization_];
    rect = [view convertRect:rect fromView:cardZ];
    return rect;
}

- (CGRect)func_convertRect:(CGRect)rect fromViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *)self) convertRect:rect fromWindow:nil];
        }else {
            return [self convertRect:rect fromView:nil];
        }
    }
    
    UIWindow *globalizationT = [view isKindOfClass:[UIWindow class]] ? (id)view : view.window;
    UIWindow *cardy = [self isKindOfClass:[UIWindow class]] ? (id)self : self.window;
    if ((!globalizationT || !cardy) || (globalizationT == cardy)) return [self convertRect:rect fromView:view];
    rect = [globalizationT convertRect:rect fromView:view];
    rect = [cardy convertRect:rect fromWindow:globalizationT];
    rect = [self convertRect:rect fromView:cardy];
    return rect;
}

/// 添加点击手势
/// - Parameter block: 点击回调
- (void)func_addTapActionWithBlock:(GestureActionBlock)block {
    UITapGestureRecognizer *picker = objc_getAssociatedObject(self, &kActionHandlerTapGestureKey);
    if (!picker) {
        picker = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleActionForTapGesture:)];
        [self addGestureRecognizer:picker];
        objc_setAssociatedObject(self, &kActionHandlerTapGestureKey, picker, OBJC_ASSOCIATION_RETAIN);
    }
    objc_setAssociatedObject(self, &kActionHandlerTapBlockKey, block, OBJC_ASSOCIATION_COPY);
}

/// 添加长按手势
/// - Parameter block: 长按回调
- (void)func_addLongPressActionWithBlock:(GestureActionBlock)block {
    UILongPressGestureRecognizer *pickerF = objc_getAssociatedObject(self, &kActionHandlerLongPressGestureKey);
    if (!pickerF) {
        pickerF = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleActionForLongPressGesture:)];
        [self addGestureRecognizer:pickerF];
        objc_setAssociatedObject(self, &kActionHandlerLongPressGestureKey, pickerF, OBJC_ASSOCIATION_RETAIN);
    }
    objc_setAssociatedObject(self, &kActionHandlerLongPressBlockKey, block, OBJC_ASSOCIATION_COPY);
}

/// 通过xib加载
+ (instancetype)func_viewFromXib {
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].firstObject;
}


-(void)func_bezierPathByRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii {
    UIBezierPath *limit = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:cornerRadii];
    CAShapeLayer *sheet = [[CAShapeLayer alloc] init];
    sheet.frame = self.bounds;
    sheet.path = limit.CGPath;
    self.layer.mask = sheet;
}


-(CGPathRef _Nullable)func_CGPathCreateWithRoundedRect:(CGRect)bounds andCornerRadii:(CornerRadiiType)cornerRadii{
    const CGFloat maximum = CGRectGetMinX(bounds);
    const CGFloat milliseconds = CGRectGetMinY(bounds);
    const CGFloat thumb = CGRectGetMaxX(bounds);
    const CGFloat header = CGRectGetMaxY(bounds);
    
    const CGFloat target = maximum + cornerRadii.topLeft;
    const CGFloat reusable = milliseconds + cornerRadii.topLeft;
    
    const CGFloat random = thumb - cornerRadii.topRight;
    const CGFloat intimacy = milliseconds + cornerRadii.topRight;
    
    const CGFloat finished = maximum + cornerRadii.bottomLeft;
    const CGFloat publish = header - cornerRadii.bottomLeft;
    
    const CGFloat tip = thumb - cornerRadii.bottomRight;
    const CGFloat havin = header - cornerRadii.bottomRight;
    
    CGMutablePathRef curZ = CGPathCreateMutable();
    CGPathAddArc(curZ, NULL, target, reusable, cornerRadii.topLeft, M_PI, 3 * M_PI_2, NO);
    CGPathAddArc(curZ, NULL, random , intimacy, cornerRadii.topRight, 3 * M_PI_2, 0, NO);
    CGPathAddArc(curZ, NULL, tip, havin, cornerRadii.bottomRight, 0, M_PI_2, NO);
    CGPathAddArc(curZ, NULL, finished, publish, cornerRadii.bottomLeft, M_PI_2,M_PI, NO);
    CGPathCloseSubpath(curZ);
    return curZ;
}

-(BOOL)func_cornerRadiiEqualTo:(CornerRadiiType)lhs andRhs:(CornerRadiiType)rhs {
    return lhs.topLeft == rhs.topRight
    && lhs.topRight == rhs.topRight
    && lhs.bottomLeft == rhs.bottomLeft
    && lhs.bottomRight == lhs.bottomRight;
}


/// 生成圆角
/// - Parameters:
///   - topLeft: 左上
///   - topRight: 右上
///   - bottomLeft: 左下
///   - bottomRight: 右下
+(CornerRadiiType)func_cornerRadiiMakeWithTopLeft:(CGFloat)topLeft withTopRight:(CGFloat)topRight withBottomLeft:(CGFloat)bottomLeft withBottomRight:(CGFloat)bottomRight {
    return (CornerRadiiType) {
        topLeft,
        topRight,
        bottomLeft,
        bottomRight,
    };
}

/// 设置圆角
/// - Parameter cornerRadii: 圆角配置
-(void)func_bezierPathByRoundingCornerRadii:(CornerRadiiType)cornerRadii {
    if (![self func_cornerRadiiEqualTo:cornerRadii andRhs:[self cornerRadiiNull]]) {
        CAShapeLayer *starting = (CAShapeLayer *)self.layer.mask;
        CGPathRef horizontally = CGPathCreateCopy(starting.path);
        CGPathRef curX = [self func_CGPathCreateWithRoundedRect:self.bounds andCornerRadii:cornerRadii];
        if (!CGPathEqualToPath(horizontally, curX)) {
            [starting removeAnimationForKey:@"path"];
            CAShapeLayer *sheet5 = [CAShapeLayer layer];
            sheet5.path = curX;
            self.layer.mask = sheet5;
            CAAnimation *circle = [self.layer animationForKey:@"bounds.size"];
            if (circle) {
                CABasicAnimation *esult = [CABasicAnimation animationWithKeyPath:@"path"];
                esult.duration = circle.duration;
                esult.fillMode = circle.fillMode;
                esult.timingFunction = circle.timingFunction;
                esult.fromValue = (__bridge id _Nullable)(horizontally);
                esult.toValue = (__bridge id _Nullable)(curX);
                [sheet5 addAnimation:esult forKey:@"path"];
            }
        }
        
        CGPathRelease(horizontally);
        CGPathRelease(curX);
    }
}

/// 点击手势回调
- (void)handleActionForTapGesture:(UITapGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        GestureActionBlock callx = objc_getAssociatedObject(self, &kActionHandlerTapBlockKey);
        if (callx) {
            callx(gesture);
        }
    }
}

/// 长按手势回调
- (void)handleActionForLongPressGesture:(UITapGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        GestureActionBlock callxw = objc_getAssociatedObject(self, &kActionHandlerLongPressBlockKey);
        if (callxw) {
            callxw(gesture);
        }
    }
}

/// 移除所有长按手势
- (void)func_removeAllLongPressGesture {
    [[self gestureRecognizers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIGestureRecognizer class]]) {
            UIGestureRecognizer *message = (UIGestureRecognizer *) obj;
            [self removeGestureRecognizer:message];
        }
    }];
}

/// 移除所有子视图
- (void)func_removeAllSubviews {
    while (self.subviews.count) {
        [self.subviews.lastObject removeFromSuperview];
    }
}

/// 当前view的控制器（可能为空）
- (UIViewController *)func_viewController {
    for (UIView *view = self; view; view = view.superview) {
        UIResponder *domain = [view nextResponder];
        if ([domain isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)domain;
        }
    }
    return nil;
}


/// 设置渐变模式
- (void)func_setGradientView {
    CAGradientLayer *swiftD = [CAGradientLayer layer];
    swiftD.frame = self.bounds;
    
    swiftD.colors = @[(__bridge id)[[UIColor clearColor] colorWithAlphaComponent:0].CGColor,
                      (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor];
    swiftD.startPoint = CGPointMake(0, 0);
    swiftD.endPoint = CGPointMake(0, 0.15);
    [self.layer setMask:swiftD];
}

/// 设置水平渐变
- (void)func_setHorizontalGradientView {
    CAGradientLayer *swift = [CAGradientLayer layer];
    swift.frame = self.bounds;
    
    swift.colors = @[(__bridge id)[[UIColor clearColor] colorWithAlphaComponent:0].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:1].CGColor,
                     (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:0].CGColor];
    swift.startPoint = CGPointMake(0, 0);
    swift.endPoint = CGPointMake(1, 0);
    swift.locations = @[@(0.05),@(0.1),@(0.2),@(0.3),@(0.4),@(0.5),@(0.6),@(0.7),@(0.8),@(0.9),@(0.95)];
    [self.layer setMask:swift];
}


/// 设置卡片模式
/// - Parameters:
///   - color: 阴影颜色
///   - offset: 阴影偏移
///   - radius: 阴影圆角
///   - shadowOpacity: 阴影透明度
- (void)func_setLayerShadow:(UIColor*)color offset:(CGSize)offset radius:(CGFloat)radius shadowOpacity:(CGFloat)shadowOpacity {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = radius;
    
    self.layer.shadowOpacity = shadowOpacity;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}


/// 添加背景渐变色
/// - Parameters:
///   - colors: 渐变色数组
///   - direction: 方向
-(void)func_addGradientLayerWithColors:(NSArray <UIColor *>*)colors withDirection:(GradientDirection)direction {
    CAGradientLayer *layer = [CAGradientLayer layer];
    
    NSMutableArray *msg = @[].mutableCopy;
    for (UIColor *color in colors) {
        [msg addObject:(__bridge id)color.CGColor];
    }
    layer.colors = msg;
    
    CGPoint startPoint;
    CGPoint endPoint;
    
    switch (direction) {
        case GradientDirectionHorizontal: {
            startPoint = CGPointMake(0, 0);
            endPoint = CGPointMake(1.0, 0);
        }
            break;
            
        case GradientDirectionVertical: {
            startPoint = CGPointMake(0, 0);
            endPoint = CGPointMake(0, 1.0);
        }
            break;
            
        case GradientDirectionTlToRb: {
            startPoint = CGPointMake(0, 0);
            endPoint = CGPointMake(1.0, 1.0);
        }
            break;
            
        default:
            break;
    }
    layer.startPoint= startPoint;
    layer.endPoint= endPoint;
    layer.frame = self.bounds;
    [self.layer insertSublayer:layer atIndex:0];
}


//MARK: - Setter

-(void)setShadowRadius:(CGFloat)shadowRadius {
    self.layer.shadowRadius = shadowRadius;
}

-(void)setBorderWidth:(CGFloat)bWidth {
    if (bWidth) {
        self.layer.borderWidth = bWidth;
    }
}

-(void)setRadius:(CGFloat)radius {
    if (radius) {
        [self.layer setCornerRadius:radius];
    }
}

-(void)setBorderColor:(UIColor *)bColor {
    if (bColor) {
        self.layer.borderColor = bColor.CGColor;
    }
}

-(void)setClipRadius:(CGFloat)clipRadius {
    if (clipRadius) {
        [self.layer setCornerRadius:clipRadius];
        [self.layer setMasksToBounds:YES];
    }
}

-(void)setShadowColor:(UIColor *)shadowColor {
    if (shadowColor) {
        self.layer.shadowColor = shadowColor.CGColor;
        self.layer.shadowOpacity = 0.5 ;
    }
}

-(void)setShadowOffset:(CGSize)shadowOffset {
    if (shadowOffset.height || shadowOffset.width) {
        self.layer.shadowOffset = shadowOffset;
    }
}

-(void)setShadowOpacity:(CGFloat)shadowOpacity {
    if (shadowOpacity) {
        self.layer.shadowOpacity = shadowOpacity ;
    }
}

- (void)func_removeAllTapGesture {
    [[self gestureRecognizers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIGestureRecognizer class]]) {
            UIGestureRecognizer *messageG = (UIGestureRecognizer *) obj;
            [self removeGestureRecognizer:messageG];
        }
    }];
}


/// 高度
- (CGFloat)height {
    return self.frame.size.height;
}
- (void)setHeight:(CGFloat)height {
    CGRect cur = self.frame;
    cur.size.height = height;
    [self setFrame:cur];
}

/// 大小
- (CGSize)size {
    return [self frame].size;
}
- (void)setSize:(CGSize)size {
    CGRect curx = self.frame;
    curx.size = size;
    [self setFrame:curx];
}

/// 位置
- (CGPoint)position {
    return self.frame.origin;
}
- (void)setPosition:(CGPoint)position {
    CGRect curq = self.frame;
    curq.origin = position;
    [self setFrame:curq];
}

/// 宽度
- (CGFloat)width {
    return self.frame.size.width;
}
- (void)setWidth:(CGFloat)width {
    CGRect curL = self.frame;
    curL.size.width = width;
    [self setFrame:curL];
}

/// 底部
- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}
- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    
    self.frame = frame;
}

/// x
- (CGFloat)left {
    return self.frame.origin.x;
}
- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

/// 可见性
- (BOOL)visible {
    return !self.hidden;
}
- (void)setVisible:(BOOL)visible {
    self.hidden=!visible;
}

/// 上
- (CGFloat)top {
    return self.frame.origin.y;
}
- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

/// 右
- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}
- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

/// 左
- (CGFloat)x {
    return self.frame.origin.x;
}
- (void)setX:(CGFloat)x {
    CGRect curj = self.frame;
    curj.origin.x = x;
    [self setFrame:curj];
}

/// y
- (CGFloat)y {
    return self.frame.origin.y;
}
- (void)setY:(CGFloat)y {
    CGRect cur7 = self.frame;
    cur7.origin.y = y;
    [self setFrame:cur7];
}


/// 移除所有子视图
-(void)removeAllSubViews {
    for (UIView *subview in self.subviews){
        [subview removeFromSuperview];
    }
}

/// 键盘升降观察
/// - Parameter changeHandler: 键盘高度改变的回调
- (void)tx_observeKeyboardOnChange:(void(^)(CGFloat keyboardTop, CGFloat height))changeHandler {
    __weak __typeof(self) wSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillChangeFrameNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        double animDuration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        changeHandler(endFrame.origin.y, endFrame.size.height);
        [UIView animateWithDuration:animDuration animations:^{
            [wSelf layoutIfNeeded];
        }];
    }];
}

/// 转换为图片
- (UIImage*)toImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    
    CGContextRef coloro = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:coloro];
    UIImage* smallImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return smallImage;
}

/// 设置背景图片
/// - Parameter image: 图片
- (void)setBackgroundImage:(UIImage *)image {
    UIGraphicsBeginImageContext(self.frame.size);
    [image drawInRect:self.bounds];
    UIImage *finishedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.backgroundColor = [UIColor colorWithPatternImage:finishedImage];
}


@end
