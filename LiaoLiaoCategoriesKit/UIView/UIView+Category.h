//
//  CategoriesKit
//  UIView+Category.h
//
//  Created by 清风徐来 on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * 手势点击block
 */
typedef void (^GestureActionBlock)(UIGestureRecognizer *gestureRecoginzer);

#define kScaleX [UIScreen mainScreen].bounds.size.width / 375
#define kScaleY [UIScreen mainScreen].bounds.size.height / 667

/// 自适应布局（根据屏幕）
/// - Parameters:
///   - x: x
///   - y: y
///   - width: 宽度
///   - height: 高度
CG_INLINE CGRect CGRectMake_Auto(CGFloat x, CGFloat y, CGFloat width, CGFloat height) {
    CGRect rect;
    rect.origin.x = x * kScaleX;
    rect.origin.y = y * kScaleY;
    rect.size.width = width * kScaleX;
    rect.size.height = height * kScaleY;
    
    return rect;
}

/**
 * 渐变层渲染方向
 */
typedef NS_ENUM(int, GradientDirection) {
    GradientDirectionHorizontal = 1, // 水平
    GradientDirectionVertical = 2,  // 垂直
    GradientDirectionTlToRb = 3 // 左上到右下
};

/**
 * 阴影类型
 */
typedef NS_ENUM(NSInteger , ShadowPathType) {
    ShadowPathTop    = 1,  // 上
    ShadowPathBottom = 2,  // 下
    ShadowPathLeft   = 3, // 左
    ShadowPathRight  = 4, // 右
    ShadowPathCommon = 5,
    ShadowPathAround = 6,
};

/**
 * 自定义圆角位置
 */
typedef struct {
    CGFloat topLeft;
    CGFloat topRight;
    CGFloat bottomLeft;
    CGFloat bottomRight;
} CornerRadiiType;


@interface UIView (Category)

/// 通过xib加载
+ (instancetype)func_viewFromXib;

/// 添加点击手势
/// - Parameter block: 点击回调
- (void)func_addTapActionWithBlock:(GestureActionBlock)block;

/// 移除所有点击手势
- (void)func_removeAllTapGesture;

/// 添加长按手势
/// - Parameter block: 长按回调
- (void)func_addLongPressActionWithBlock:(GestureActionBlock)block;

/// 移除所有长按手势
- (void)func_removeAllLongPressGesture;

/// 设置圆角
/// - Parameter cornerRadii: 圆角配置
-(void)func_bezierPathByRoundingCornerRadii:(CornerRadiiType)cornerRadii;

/// 生成圆角
/// - Parameters:
///   - topLeft: 左上
///   - topRight: 右上
///   - bottomLeft: 左下
///   - bottomRight: 右下
+(CornerRadiiType)func_cornerRadiiMakeWithTopLeft:(CGFloat)topLeft withTopRight:(CGFloat)topRight withBottomLeft:(CGFloat)bottomLeft withBottomRight:(CGFloat)bottomRight;

/// 添加背景渐变色
/// - Parameters:
///   - colors: 渐变色数组
///   - direction: 方向
-(void)func_addGradientLayerWithColors:(NSArray <UIColor *>*)colors withDirection:(GradientDirection)direction;

/// 指定区域透明（抠图）
/// - Parameter rect: 指定区域
-(void)func_setAlphaComponent:(CGRect)rect;

/// 设置渐变模式
- (void)func_setGradientView;

/// 设置水平渐变
- (void)func_setHorizontalGradientView;

/// 截图
- (nullable UIImage *)func_snapshotImage;

/// 截图
/// - Parameter afterUpdates: 是否等待view更新完成
- (nullable UIImage *)func_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates;

/// 截图生成PDF
- (nullable NSData *)func_snapshotPDF;

/// 设置卡片模式
/// - Parameters:
///   - color: 阴影颜色
///   - offset: 阴影偏移
///   - radius: 阴影圆角
///   - shadowOpacity: 阴影透明度
- (void)func_setLayerShadow:(UIColor*)color offset:(CGSize)offset radius:(CGFloat)radius shadowOpacity:(CGFloat)shadowOpacity;

/// 添加阴影
/// - Parameters:
///   - shadowColor: 阴影颜色
///   - shadowOpacity: 阴影透明度
///   - shadowRadius: 阴影半径
///   - shadowPathType: 阴影类型
///   - shadowPathWidth: 阴影宽度
- (void)func_viewShadowPathWithColor:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowPathType:(ShadowPathType)shadowPathType shadowPathWidth:(CGFloat)shadowPathWidth;

/// 移除所有子视图
- (void)func_removeAllSubviews;

/// 移除所有子视图
- (void)removeAllSubViews;

/// 设置背景图片
/// - Parameter image: 图片
- (void)setBackgroundImage:(UIImage*)image;

/// 转换为图片
- (UIImage*)toImage;

/// 键盘升降观察
/// - Parameter changeHandler: 键盘高度改变的回调
- (void)tx_observeKeyboardOnChange:(void(^)(CGFloat keyboardTop, CGFloat height))changeHandler;


/// view变换
- (CGPoint)func_convertPoint:(CGPoint)point toViewOrWindow:(nullable UIView *)view;
- (CGPoint)func_convertPoint:(CGPoint)point fromViewOrWindow:(nullable UIView *)view;
- (CGRect)func_convertRect:(CGRect)rect toViewOrWindow:(nullable UIView *)view;
- (CGRect)func_convertRect:(CGRect)rect fromViewOrWindow:(nullable UIView *)view;

/// 当前view的控制器（可能为空）
@property (nonatomic, nullable, readonly) UIViewController *func_viewController;

///xib设置属性
@property (nonatomic, assign) IBInspectable CGFloat clipRadius;
@property (nonatomic, assign) IBInspectable CGFloat radius;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
@property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
@property (nonatomic, strong) IBInspectable UIColor *shadowColor;
@property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
@property (nonatomic, assign) IBInspectable CGSize shadowOffset;

/// 普通属性
@property CGPoint position;
@property CGFloat x;
@property CGFloat y;
@property CGFloat top;
@property CGFloat bottom;
@property CGFloat left;
@property CGFloat right;
@property BOOL    visible;
@property CGSize  size;
@property CGFloat width;
@property CGFloat height;

@end

NS_ASSUME_NONNULL_END
